<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Journals';
?>

<div class="row">
    <div class="col-lg-12">
        <h1>Journals <a href="<?=Url::to(['journals/create'])?>" class="btn btn-success pull-right">Create</a></h1>
        <?php
        if (Yii::$app->session->hasFlash('success')) {
        ?>
            <div class="alert alert-success" role="alert"><?=Yii::$app->session->getFlash('success')?></div>
        <?php
        }
        ?>
    </div>
    <div class="col-lg-12">
        <?= yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'label' => 'Id',
                    'attribute' => 'id',
                    'contentOptions' => ['style'=>'width: 10%;']
                ],
                'name',
                [
                    'label' => 'Created',
                    'format' => 'datetime',
                    'attribute' => 'created',
                    'contentOptions' => ['style'=>'width: 20%;']
                ],
                [
                    'label' => 'Actions',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return '<div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="'.Url::to(['//journals/edit', 'id' => $data->id]).'">Edit</a></li>
                            </ul>
                        </div>';
                    },
                    'contentOptions' => ['style'=>'width: 10%;']
                ],
            ],
        ]) ?>
    </div>
</div>
