<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Headnotes';
?>

<div class="row">
    <div class="col-lg-12">
        <h1>Headnotes
        <?php
        if (Yii::$app->session->hasFlash('success')) {
        ?>
            <div class="alert alert-success" role="alert"><?=Yii::$app->session->getFlash('success')?></div>
        <?php
        }
        ?>
    </div>
    <div class="col-lg-12">
        <?= yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                [
                    'label' => 'Judgement',
                    'value' => function ($data) {
                        return $data->judgement->type->code.' '.$data->judgement->number.' '.$data->judgement->year;
                    },
                    'contentOptions' => ['style'=>'width: 30%;']
                ],
                [
                    'label' => 'Created By',
                    'value' => function ($data) {
                        return $data->createdBy->first_name.' '.$data->createdBy->last_name;
                    },
                    'contentOptions' => ['style'=>'width: 30%;']
                ],
                [
                    'label' => 'Created On',
                    'format' => 'datetime',
                    'attribute' => 'created',
                    'contentOptions' => ['style'=>'width: 20%;']
                ],
                [
                    'label' => 'Actions',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return '<div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="'.Url::to(['//headnotes/edit', 'id' => $data->id]).'">Edit</a></li>
                            </ul>
                        </div>';
                    },
                    'contentOptions' => ['style'=>'width: 10%;']
                ],
            ],
        ]) ?>
    </div>
</div>
