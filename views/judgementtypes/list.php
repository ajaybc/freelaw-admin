<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Judgement Types';
?>

<div class="row">
    <div class="col-lg-12">
        <h1>Judgement Types <a href="<?=Url::to(['judgementtypes/create'])?>" class="btn btn-success pull-right">Create</a></h1>
        <?php
        if (Yii::$app->session->hasFlash('success')) {
        ?>
            <div class="alert alert-success" role="alert"><?=Yii::$app->session->getFlash('success')?></div>
        <?php
        }
        ?>
    </div>
    <div class="col-lg-12">
        <?= yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'name',
                [
                    'label' => 'Actions',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return '<div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="'.Url::to(['//judgementtypes/edit', 'id' => $data->id]).'">Edit</a></li>
                            </ul>
                        </div>';
                    },
                    'contentOptions' => ['style'=>'width: 10%;']
                ],
            ],
        ]) ?>
    </div>
</div>
