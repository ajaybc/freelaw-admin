<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Users';
?>

<div class="row">
    <div class="col-lg-12">
        <h1>Users <a href="<?=Url::to(['users/create'])?>" class="btn btn-success pull-right">Create</a></h1>
        <?php
        if (Yii::$app->session->hasFlash('success')) {
        ?>
            <div class="alert alert-success" role="alert"><?=Yii::$app->session->getFlash('success')?></div>
        <?php
        }
        ?>
    </div>
    <div class="col-lg-12">
        <?= yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'first_name',
                'last_name',
                'email',
                [
                    'label' => 'Created',
                    'format' => 'datetime',
                    'attribute' => 'created',
                    'contentOptions' => ['style'=>'width: 20%;']
                ],
                [
                    'label' => 'Actions',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return '<div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="'.Url::to(['//users', 'toggle' => $data->id]).'">'.(($data->status == 'active')?'Deactivate':'Activate').'</a></li>
                            </ul>
                        </div>';
                    }
                ],
            ],
        ]) ?>
    </div>
</div>
