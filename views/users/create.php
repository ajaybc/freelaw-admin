<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */

$this->title = 'Users';
?>

<div class="row">
    <div class="col-lg-12">
        <h1>Create User</h1>
    </div>
    <div class="col-lg-12">
        <?php $form = ActiveForm::begin([
            'id' => 'create-form',
            'options' => ['class' => ''],
            'fieldConfig' => [
                //'template' => "<div class=\"col-lg-12\">{label}\n{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
                'inputOptions' => ['class' => 'form-control'],
            ],
        ]); ?>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <?= $form->field($model, 'last_name')->textInput() ?>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <?= $form->field($model, 'email')->input('email') ?>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <?= $form->field($model, 'password')->passwordInput() ?>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <?= $form->field($model, 'verification')->checkbox(['uncheck' => 'unverifeid', 'value' => 'verified']); ?>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-12">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
    </div>
</div>
