<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Freelaw', //
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = [
            'label' => 'Manage',
            'items' => [
                [
                    'label' => 'Judges',
                    'url' => ['/judges'],
                ],
                [
                    'label' => 'Courts',
                    'url' => ['/courts'],
                ],
                [
                    'label' => 'Judgement Types',
                    'url' => ['/judgementtypes'],
                ],
                [
                    'label' => 'Judgements',
                    'url' => ['/judgements'],
                ],
                [
                    'label' => 'Headnotes',
                    'url' => ['/headnotes'],
                ],
                [
                    'label' => 'Acts',
                    'url' => ['/books'],
                ],
                [
                    'label' => 'Journals',
                    'url' => ['/journals'],
                ],
            ],
        ];
        $menuItems[] = ['label' => 'Logout', 'url' => ['/site/logout']];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Freelaw <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
