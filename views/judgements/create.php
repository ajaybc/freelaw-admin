<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
/* @var $this yii\web\View */
app\assets\JudgementAsset::register($this);
$this->title = 'Judgements';
?>

<div class="row ng-cloak" ng-app="freelaw" ng-controller="judgementController" <?=($judgementId)?'ng-init="preloadData($judgeId)"':''?>>
    <div class="col-lg-12">
        <?php
        if ($judgementId) {
        ?>
            <h1>Edit Judgement</h1>
        <?php
        } else {
        ?>
            <h1>Create Judgement</h1>
        <?php
        }
        ?>
    </div>
    <div class="col-lg-12">
        <form ng-submit="submit($event)" name="form" novalidate>
            <div class="loading" ng-show="showLoading"></div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="form-group" ng-class="{ 'has-error': (form.court_id.$touched || form.$submitted) && form.court_id.$invalid}">
                        <label class="control-label">Court</label>
                        <select ng-model="formData.court_id" class="form-control" required name="court_id">
                            <option value="">Please select a court</option>
                            <?php
                            if ($courts) {
                                foreach ($courts as  $court) {
                                    echo '<option value="'.$court['id'].'">'.$court['name'].'</option>';
                                }
                            }
                            ?>
                        </select>
                        <div class="help-block" ng-messages="form.court_id.$error" ng-if="(form.court_id.$touched || form.$submitted)">
                            <p ng-message="required">Please select a court</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="form-group" ng-class="{ 'has-error': (form.type_id.$touched || form.$submitted) && form.type_id.$invalid}">
                        <label class="control-label">Case Type</label>
                        <select ng-model="formData.type_id" class="form-control" required name="type_id">
                            <option value="">Please select a case type</option>
                            <?php
                            if ($judgementTypes) {
                                foreach ($judgementTypes as $judgementType) {
                                    echo '<option value="'.$judgementType['id'].'">'.$judgementType['name'].'</option>';
                                }
                            }
                            ?>
                        </select>
                        <div class="help-block" ng-messages="form.type_id.$error" ng-if="(form.type_id.$touched || form.$submitted)">
                            <p ng-message="required">Please select a case type</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="form-group" ng-class="{ 'has-error': (form.number.$touched || form.$submitted) && form.number.$invalid}">
                        <label class="control-label">Case Number</label>
                        <input type="text" class="form-control" ng-model="formData.number" required name="number" pattern="^\d*$">
                        <div class="help-block" ng-messages="form.number.$error" ng-if="(form.number.$touched || form.$submitted)">
                            <p ng-message="required">Please enter a case number</p>
                            <p ng-message="pattern">Please enter a valid case number</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="form-group" ng-class="{ 'has-error': (form.year.$touched || form.$submitted) && form.year.$invalid}">
                        <label class="control-label">Case Year</label>
                        <select ng-model="formData.year" class="form-control" required name="year">
                            <option value="">Please select a case year</option>
                            <?php
                            $currentYear = date('Y');
                            for ($i = $currentYear; $i > ($currentYear - 200); $i--) {
                                echo '<option value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select>
                        <div class="help-block" ng-messages="form.year.$error" ng-if="(form.year.$touched || form.$submitted)">
                            <p ng-message="required">Please select a year</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="form-group clearfix" ng-class="{ 'has-error': (form.decided_on.$touched || form.$submitted) && form.decided_on.$invalid}">
                        <label class="control-label">Decided On</label>
                        <datepicker date-format="yyyy-MM-dd" date-max-limit="<?=date('Y-m-d')?>">
                            <input ng-model="formData.decided_on" type="text" class="form-control" name="decided_on" required/>
                        </datepicker>
                        <div class="help-block" ng-messages="form.decided_on.$error" ng-if="(form.decided_on.$touched || form.$submitted)">
                            <p ng-message="required">Please select a date</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group" ng-class="{ 'has-error': (form.appellants.$touched || form.$submitted) && form.appellants.$invalid}">
                        <label class="control-label">Appellants</label>
                        <input type="text" class="form-control" ng-model="formData.appellants" name="appellants" required>
                        <div class="help-block" ng-messages="form.appellants.$error" ng-if="(form.appellants.$touched || form.$submitted)">
                            <p ng-message="required">Please enter the appellants</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group" ng-class="{ 'has-error': (form.respondents.$touched || form.$submitted) && form.respondents.$invalid}">
                        <label class="control-label">Respondents</label>
                        <input type="text" class="form-control" ng-model="formData.respondents" name="respondents" required>
                        <div class="help-block" ng-messages="form.respondents.$error" ng-if="(form.respondents.$touched || form.$submitted)">
                            <p ng-message="required">Please select the respondent</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="form-group">
                        <label class="control-label">Judges</label>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <input type="text" class="form-control" ng-model="judgeFilter" placeholder="Search / Filter Judges">
                                <a class="pull-right" ng-click="clearJudgeFilter()">Clear</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <ul class="select-wrap">
                                    <li ng-repeat="judge in judges | filter:judgeFilter" class="select-item">
                                        <a ng-click="clickJudge(judge.id)">
                                            <i class="fa fa-square-o" aria-hidden="true" ng-if="formData.judge_ids.indexOf(judge.id) == -1"></i>
                                            <i class="fa fa-check" aria-hidden="true" ng-if="formData.judge_ids.indexOf(judge.id) > -1"></i>
                                            <!-- <i class="fa fa-square-o" aria-hidden="true"></i> -->
                                            {{judge.first_name + ' ' + judge.last_name}}
                                        </a>
                                    </li>
                                </ul>
                                {{formData.judge_ids.length}} selected <a class="pull-right" ng-click="unselectJudges($event)">None</a><span class="pull-right">&nbsp;&nbsp; | &nbsp;&nbsp;</span> <a class="pull-right" ng-click="selectAllJudges($event)">All</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group">
                        <fieldset>
                            <legend>Acts / Rules / Orders</legend>
                            <div class="row" ng-repeat="act in formData.acts track by $index" style="margin-bottom: 20px;">
                                <div class="col-lg-12 col-md-12 col-sm-12 form-group" ng-class="{ 'has-error': (form['acts_' + $index + '_book_id'].$touched || form.$submitted) && form['acts_' + $index + '_book_id'].$invalid}">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <label class="control-label">Act</label>
                                            <select class="form-control" ng-model="act.book_id" required name="acts_{{$index}}_book_id">
                                                <option value="">Please select an act</option>
                                                <?php
                                                if ($books) {
                                                    foreach ($books as  $book) {
                                                        echo '<option value="'.$book['id'].'">'.$book['title'].'</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <div class="help-block" ng-messages="form['acts_' + $index + '_book_id'].$error" ng-if="(form['acts_' + $index + '_book_id'].$touched || form.$submitted)">
                                                <p ng-message="required">Please select an act</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <a class="pull-right" ng-click="removeAct($index)">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 form-group" ng-class="{ 'has-error': (form['acts_' + $index + '_year'].$touched || form.$submitted) && form['acts_' + $index + '_year'].$invalid}">
                                    <label class="control-label">Year</label>
                                    <input type="text" class="form-control" pattern="^\d{4}$" ng-model="act.year" name="acts_{{$index}}_year" required maxlength="4" />
                                    <div class="help-block" ng-messages="form['acts_' + $index + '_year'].$error" ng-if="(form['acts_' + $index + '_year'].$touched || form.$submitted)">
                                        <p ng-message="required">Please enter a year</p>
                                        <p ng-message="pattern">Please enter a valid year</p>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 form-group">
                                    <label class="control-label">Chapter</label>
                                    <input type="text" class="form-control" ng-model="act.chapter"/>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <label class="control-label">Part</label>
                                    <input type="text" class="form-control" ng-model="act.part"/>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <label class="control-label">Section</label>
                                    <input type="text" class="form-control" ng-model="act.section"/>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <label class="control-label">Order Number</label>
                                    <input type="text" class="form-control" ng-model="act.order_number"/>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <label class="control-label">Rule</label>
                                    <input type="text" class="form-control" ng-model="act.rule"/>
                                </div>
                            </div>
                            <div ng-if="formData.acts.length == 0">
                                No acts selected
                            </div>
                            <br/>
                            <a ng-click="addAct()">+ Add Act</a>
                        </div>
                    </fieldset>
                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="form-group" ng-class="{ 'has-error': (form.disposition.$touched || form.$submitted) && form.disposition.$invalid}">
                        <label class="control-label">Disposition</label>
                        <select ng-model="formData.disposition" class="form-control" required name="disposition">
                            <option value="">Please select the case disposition</option>
                            <option value="dismissed">Dismissed</option>
                            <option value="allowed">Allowed</option>
                        </select>
                        <div class="help-block" ng-messages="form.disposition.$error" ng-if="(form.disposition.$touched || form.$submitted)">
                            <p ng-message="required">Please select the disposition</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group">
                        <label class="control-label">Catch Words</label>
                        <input type="text" class="form-control" ng-model="formData.catchwords" name="catchwords">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group" ng-class="{ 'has-error': (form.subject.$touched || form.$submitted) && form.subject.$invalid}">
                        <label class="control-label">Subject</label>
                        <input type="text" class="form-control" ng-model="formData.subject" required maxlength="500" name="subject">
                        <div class="help-block" ng-messages="form.subject.$error" ng-if="(form.subject.$touched || form.$submitted)">
                            <p ng-message="required">Please enter the subject</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group" ng-class="{ 'has-error': (form.judgement.$touched || form.$submitted) && form.judgement.$invalid}">
                        <label class="control-label">Judgement</label>
                        <textarea required ui-tinymce="tinymceOptions" ng-model="formData.judgement" name="judgement"></textarea>
                        <div class="help-block" ng-messages="form.judgement.$error" ng-if="(form.judgement.$touched || form.$submitted)">
                            <p ng-message="required">Please enter the judgement</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group">
                        <fieldset>
                            <legend>Citations</legend>
                            <div class="row" ng-repeat="citation in formData.citations track by $index" style="margin-bottom: 20px;">
                                <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 form-group" ng-class="{'has-error': (form['citations_' + {{$index}}+ '_journal_id'].$touched || form.$submitted) && form['citations_' + $index + '_journal_id'].$invalid}">
                                            <label class="control-label">Journal</label>
                                            <select class="form-control" ng-model="citation.journal_id" name="citations_{{$index}}_journal_id" required>
                                                <option value="">Please select a journal</option>
                                                <?php
                                                if ($journals) {
                                                    foreach ($journals as  $journal) {
                                                        echo '<option value="'.$journal['id'].'">'.$journal['name'].'</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                            <div class="help-block" ng-messages="form['citations_' + $index + '_journal_id'].$error" ng-if="(form['citations_' + $index + '_journal_id'].$touched || form.$submitted)">
                                                <p ng-message="required">Please select a journal</p>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <a class="pull-right" ng-click="removeCitation($index)">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 form-group" ng-class="{ 'has-error': (form['citations_' + $index + '_year'].$touched || form.$submitted) && form['citations_' + $index + '_year'].$invalid}">
                                    <label class="control-label">Year</label>
                                    <input type="text" class="form-control" maxlength="4"  ng-model="citation.year" name="citations_{{$index}}_year" pattern="^\d{4}$" required/>
                                    <div class="help-block" ng-messages="form['citations_' + $index + '_year'].$error" ng-if="(form['citations_' + $index + '_year'].$touched || form.$submitted)">
                                        <p ng-message="required">Please enter a year</p>
                                        <p ng-message="pattern">Please enter a valid year</p>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <label class="control-label">Volume</label>
                                    <input type="text" class="form-control" ng-model="citation.volume"/>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2">
                                    <label class="control-label">Page Number</label>
                                    <input type="text" class="form-control" ng-model="citation.page"/>
                                </div>
                            </div>
                            <div ng-if="formData.citations.length == 0">
                                No citations selected
                            </div>
                            <br/>
                            <a ng-click="addCitation()">+ Add Citation</a>
                        </div>
                    </fieldset>
                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group">
                        <input type="submit" value="Submit" class="btn btn-primary" />
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
