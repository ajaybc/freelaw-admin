<?php
use yii\helpers\Url;
/* @var $this yii\web\View */
app\assets\JudgementAsset::register($this);
$this->title = 'Judgements';
?>

<div class="row">
    <div class="col-lg-12">
        <h1>Judgements <a href="<?=Url::to(['judgements/createform'])?>" class="btn btn-success pull-right">Create</a></h1>
        <?php
        if (Yii::$app->session->hasFlash('success')) {
        ?>
            <div class="alert alert-success" role="alert"><?=Yii::$app->session->getFlash('success')?></div>
        <?php
        }
        ?>
    </div>
    <div class="col-lg-12">
        <?= yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'subject',
                [
                    'label' => 'Type',
                    'value' => function ($data) {
                        return $data->type->name;
                    },
                    'contentOptions' => ['style'=>'width: 20%;']
                ],
                [
                    'label' => 'Created By',
                    'value' => function ($data) {
                        return $data->createdBy->first_name.' '.$data->createdBy->last_name;
                    },
                    'contentOptions' => ['style'=>'width: 20%;']
                ],
                [
                    'label' => 'Created On',
                    'format' => 'datetime',
                    'attribute' => 'created',
                    'contentOptions' => ['style'=>'width: 20%;']
                ],
                [
                    'label' => 'Actions',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return '<div class="btn-group">
                            <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Actions <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="'.Url::to(['//judgements/editform', 'id' => $data->id]).'">Edit</a></li>
                            </ul>
                        </div>';
                    },
                    'contentOptions' => ['style'=>'width: 10%;']
                ],
            ],
        ]) ?>
    </div>
</div>
