<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\JudgementTypes;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

class JudgementtypesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => [],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        $data = Yii::$app->request->get();

        $dataProvider = new ActiveDataProvider([
            'query' => JudgementTypes::find(),
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        /*$model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }*/
        $model = new JudgementTypes();

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            //stop(Yii::$app->request->post());
            if ($model->save()) {
                return $this->redirect('/judgementtypes',302);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'heading' => 'Add Judgement Type'
        ]);
    }

    public function actionEdit($id)
    {
        /*$model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }*/
        $model = JudgementTypes::findOne($id);
        //$model->load(Yii::$app->request->post());

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if ($model->save()) {
                return $this->redirect('/judgementtypes',302);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'heading' => 'Edit Judgement Type'
        ]);
    }
}
