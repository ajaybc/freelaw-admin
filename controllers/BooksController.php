<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Books;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

class BooksController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => [],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        $data = Yii::$app->request->get();
        // if ($data['toggle']) {
        //     $user = Judges::findOne($data['toggle']);
        //     if ($user->status == 'active') {
        //         $user->status = 'inactive';
        //         $message = 'User deactivated successfully';
        //     } else {
        //         $user->status = 'active';
        //         $message = 'User activated successfully';
        //     }
        //     $user->save();
        //     Yii::$app->getSession()->setFlash('success', $message);
        //     return $this->redirect('/users',302);
        // }

        $dataProvider = new ActiveDataProvider([
            'query' => Books::find(),
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider
        ]);
        //return $this->render('list');
    }

    public function actionCreate()
    {
        /*$model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }*/
        $model = new Books();

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if ($model->save()) {
                return $this->redirect('/books',302);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'heading' => 'Create Book'
        ]);
    }

    public function actionEdit($id)
    {
        /*$model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }*/
        $model = Books::findOne($id);
        //$model->load(Yii::$app->request->post());

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if ($model->save()) {
                return $this->redirect('/books',302);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'heading' => 'Edit Book'
        ]);
    }
}
