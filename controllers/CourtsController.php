<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Courts;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

class CourtsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => [],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionCourtsjson()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return Courts::find()->all();
    }

    public function actionIndex()
    {
        $data = Yii::$app->request->get();

        $dataProvider = new ActiveDataProvider([
            'query' => Courts::find(),
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        /*$model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }*/
        $model = new Courts();

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if ($model->save()) {
                return $this->redirect('/courts',302);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'heading' => 'Add Court'
        ]);
    }

    public function actionEdit($id)
    {
        /*$model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }*/
        $model = Courts::findOne($id);
        //$model->load(Yii::$app->request->post());

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if ($model->save()) {
                return $this->redirect('/courts',302);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'heading' => 'Edit Court'
        ]);
    }
}
