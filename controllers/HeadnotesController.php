<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Headnotes;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

class HeadnotesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => [],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        $data = Yii::$app->request->get();

        $query = Headnotes::find()->with('judgement.type', 'createdBy');
        $where = [];
        if ($data['created_by']) {
            $where['created_by'] = $data['created_by'];
        }

        if ($data['judgement_id']) {
            $where['judgement_id'] = $data['judgement_id'];
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query->where($where)
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        /*$model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }*/
        $model = new Headnotes();

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if ($model->save()) {
                return $this->redirect('/headnotes',302);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'heading' => 'Create Book'
        ]);
    }

    public function actionEdit($id)
    {
        /*$model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }*/
        $model = Headnotes::findOne($id);
        //$model->load(Yii::$app->request->post());

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if ($model->save()) {
                return $this->redirect('/headnotes',302);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'heading' => 'Edit Book'
        ]);
    }
}
