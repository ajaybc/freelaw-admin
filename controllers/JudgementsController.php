<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Courts;
use app\models\JudgementTypes;
use app\models\Judges;
use app\models\Books;
use app\models\Judgements;
use app\models\Journals;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

class JudgementsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => [],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if ($action->id == 'create' || $action->id == 'edit') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $data = Yii::$app->request->get();
        $dataProvider = new ActiveDataProvider([
            'query' => Judgements::find()->with('type', 'createdBy'),
        ]);

        //stop(Judgements::find());

        return $this->render('list', [
            'dataProvider' => $dataProvider
        ]);
        //return $this->render('list');
    }

    public function actionCreateform()
    {
        $judges = Judges::find()->all();
        $courts = Courts::find()->all();
        $journals = Journals::find()->all();
        $books = Books::find()->all();
        $judgementTypes = JudgementTypes::find()->all();
        return $this->render('create', [
            'model' => $model,
            'judgementTypes' => $judgementTypes,
            'courts' => $courts,
            'judges' => $judges,
            'books' => $books,
            'journals' => $journals,
        ]);
    }

    public function actionCreate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Judgements();
        if (Yii::$app->request->post()) {
            $response = $model->create(Yii::$app->request->post());
            if ($response['status']) {
                return [
                    'status' => 'success'
                ];
            } else {
                Yii::$app->response->statusCode = 400;
                return [
                    'status' => 'error',
                    'errors' => $response['errors']
                ];
            }
        }
        Yii::$app->response->statusCode = 400;
        return ['status' => 'error'];
    }

    public function actionEditform($id)
    {
        //stop($judgementsModel->getDetailsForEdit($id));
        $judges = Judges::find()->all();
        $courts = Courts::find()->all();
        $journals = Journals::find()->all();
        $books = Books::find()->all();
        $judgementTypes = JudgementTypes::find()->all();
        return $this->render('create', [
            'model' => $model,
            'judgementTypes' => $judgementTypes,
            'courts' => $courts,
            'judges' => $judges,
            'books' => $books,
            'journals' => $journals,
            'judgementId' => $id
        ]);
    }

    public function actionEdit($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Judgements();
        if (Yii::$app->request->post()) {
            $response = $model->edit(Yii::$app->request->post());
            if ($response['status']) {
                return [
                    'status' => 'success'
                ];
            } else {
                Yii::$app->response->statusCode = 400;
                return [
                    'status' => 'error',
                    'errors' => $response['errors']
                ];
            }
        }
        Yii::$app->response->statusCode = 400;
        return ['status' => 'error'];
    }

    public function actionEditdatajson($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $judgementsModel = new Judgements;
        return $judgementsModel->getDetailsForEdit($id);
        return [];
    }


    public function actionCasetypesjson()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'O.P' => 'ORIGINAL PETITIONS',
            'W.P(C)' => 'WRIT PETITION (CIVIL)',
            'W.A' => 'WRIT APPEAL',
            'L.A.A' => 'LAND ACQUISITION APPEAL',
            'M.A.C.A' => 'MOTOR ACCIDENTS APPEALS',
            'R.S.A' => 'REGULAR SECOND APPEAL',
            'R.F.A' => 'REGULAR FIRST APPEAL',
            'A.S' => 'APPEAL SUIT',
            'C.R.P' => 'CIVIL REVISION PETITION',
            'CRL.MC' => 'CRIMINAL MISCELLANEOUS PETITION',
            'CRL.R.P' => 'CRIMINAL REVISION PETITION',
            'CRL.A' => 'CRIMINAL APPEAL'
        ];
    }
}
