<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Journals;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;

class JournalsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => [],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    public function actionIndex()
    {
        $data = Yii::$app->request->get();

        $dataProvider = new ActiveDataProvider([
            'query' => Journals::find(),
        ]);

        return $this->render('list', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate()
    {
        $model = new Journals();

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if ($model->save()) {
                return $this->redirect('/journals',302);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'heading' => 'Create Journal'
        ]);
    }

    public function actionEdit($id)
    {
        /*$model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }*/
        $model = Journals::findOne($id);
        //$model->load(Yii::$app->request->post());

        if (Yii::$app->request->post()) {
            $model->load(Yii::$app->request->post());
            if ($model->save()) {
                return $this->redirect('/journals',302);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'heading' => 'Edit Journal'
        ]);
    }
}
