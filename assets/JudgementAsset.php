<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class JudgementAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/angular-datepicker.min.css',
        'css/site.css',
    ];
    public $js = [
        'js/libs/tinymce/tinymce.min.js',
        'js/libs/angular.min.js',
        'js/libs/angular-sanitize.min.js',
        'js/libs/angular-messages.min.js',
        'js/libs/ui-bootstrap-tpls-2.3.1.min.js',
        'js/libs/ui-tinymce/tinymce.min.js',
        'js/libs/angular-datepicker.min.js',
        'js/app/judgements.js',
        //'js/libs/ng-ui-select.min.js',
    ];
}
