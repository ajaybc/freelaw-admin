-- phpMyAdmin SQL Dump
-- version 4.6.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 18, 2016 at 11:38 AM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 7.0.14-2+deb.sury.org~trusty+1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `freelaw`
--

-- --------------------------------------------------------

--
-- Table structure for table `acts`
--

CREATE TABLE `acts` (
  `id` int(11) NOT NULL,
  `title` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `bio` text COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Admin users';

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `first_name`, `last_name`, `username`, `password`, `bio`, `created`, `modified`) VALUES
(1, 'Sarat', 'Kumar', 'admin', '$2a$06$cHomKjdrQkeSSxsKHw2TWOVDXtJvPUpioMlFMAeBDJVmDUkz2eE.K', '', '2016-05-09 15:38:51', '2016-12-18 05:51:45');

-- --------------------------------------------------------

--
-- Table structure for table `headnotes`
--

CREATE TABLE `headnotes` (
  `id` int(11) NOT NULL,
  `judgement_id` int(11) NOT NULL,
  `headnote` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `judgements`
--

CREATE TABLE `judgements` (
  `id` int(11) NOT NULL,
  `court` int(11) NOT NULL,
  `type` enum('OP','WPC','WA','LAA','MACA','RSA','RFA','AS','CRP','CRLMC','CRLRP','CRLA') COLLATE utf8_unicode_ci NOT NULL,
  `number` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `decided_on` date DEFAULT NULL,
  `appellants` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `respondents` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `judge_id` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `catchwords` text COLLATE utf8_unicode_ci NOT NULL,
  `disposition` enum('dismissed','allowed') COLLATE utf8_unicode_ci NOT NULL,
  `judgement` longtext COLLATE utf8_unicode_ci NOT NULL,
  `copyright` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `judgement_acts`
--

CREATE TABLE `judgement_acts` (
  `judgment_id` int(11) NOT NULL,
  `act_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='binding table from judgments to acts';

-- --------------------------------------------------------

--
-- Table structure for table `judges`
--

CREATE TABLE `judges` (
  `id` int(11) NOT NULL,
  `first_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Holds the list of all the judges';

--
-- Dumping data for table `judges`
--

INSERT INTO `judges` (`id`, `first_name`, `last_name`, `created`) VALUES
(1, 'Satish', 'Ninan', '2016-12-18 04:47:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acts`
--
ALTER TABLE `acts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `headnotes`
--
ALTER TABLE `headnotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `judgement_id` (`judgement_id`);

--
-- Indexes for table `judgements`
--
ALTER TABLE `judgements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `judge_id` (`judge_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `judgement_acts`
--
ALTER TABLE `judgement_acts`
  ADD UNIQUE KEY `judgment_id_2` (`judgment_id`,`act_id`),
  ADD KEY `judgment_id` (`judgment_id`),
  ADD KEY `act_id` (`act_id`);

--
-- Indexes for table `judges`
--
ALTER TABLE `judges`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acts`
--
ALTER TABLE `acts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `judgements`
--
ALTER TABLE `judgements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `judges`
--
ALTER TABLE `judges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `headnotes`
--
ALTER TABLE `headnotes`
  ADD CONSTRAINT `headnotes_ibfk_1` FOREIGN KEY (`judgement_id`) REFERENCES `judgements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `headnotes_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `admin_users` (`id`);

--
-- Constraints for table `judgements`
--
ALTER TABLE `judgements`
  ADD CONSTRAINT `judgements_ibfk_1` FOREIGN KEY (`judge_id`) REFERENCES `judges` (`id`),
  ADD CONSTRAINT `judgements_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `admin_users` (`id`);

--
-- Constraints for table `judgement_acts`
--
ALTER TABLE `judgement_acts`
  ADD CONSTRAINT `judgement_acts_ibfk_1` FOREIGN KEY (`judgment_id`) REFERENCES `judgements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `judgement_acts_ibfk_2` FOREIGN KEY (`act_id`) REFERENCES `acts` (`id`) ON DELETE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
