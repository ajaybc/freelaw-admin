<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "acts".
 *
 * @property integer $id
 * @property integer $book_id
 * @property integer $year
 * @property integer $chapter
 * @property integer $part
 * @property integer $section
 * @property integer $order_number
 * @property integer $rule
 * @property string $created
 * @property string $updated
 *
 * @property Books $book
 * @property JudgementActs[] $judgementActs
 * @property Judgements[] $judgments
 */
class Acts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'acts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_id'], 'required'],
            [['book_id', 'year', 'chapter', 'part', 'section', 'order_number', 'rule'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Books::className(), 'targetAttribute' => ['book_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'book_id' => 'Book ID',
            'year' => 'Year',
            'chapter' => 'Chapter',
            'part' => 'Part',
            'section' => 'Section',
            'order_number' => 'Order Number',
            'rule' => 'Rule',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Books::className(), ['id' => 'book_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgementActs()
    {
        return $this->hasMany(JudgementActs::className(), ['act_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgments()
    {
        return $this->hasMany(Judgements::className(), ['id' => 'judgment_id'])->viaTable('judgement_acts', ['act_id' => 'id']);
    }
}
