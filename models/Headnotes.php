<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "headnotes".
 *
 * @property integer $id
 * @property integer $judgement_id
 * @property string $headnote
 * @property integer $created_by
 * @property string $created
 * @property string $updated
 *
 * @property Judgements $judgement
 * @property AdminUsers $createdBy
 */
class Headnotes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'headnotes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judgement_id', 'headnote', 'created_by'], 'required'],
            [['judgement_id', 'created_by'], 'integer'],
            [['headnote'], 'string'],
            [['created', 'updated'], 'safe'],
            [['judgement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Judgements::className(), 'targetAttribute' => ['judgement_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => AdminUsers::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judgement_id' => 'Judgement ID',
            'headnote' => 'Headnote',
            'created_by' => 'Created By',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgement()
    {
        return $this->hasOne(Judgements::className(), ['id' => 'judgement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(AdminUsers::className(), ['id' => 'created_by']);
    }
}
