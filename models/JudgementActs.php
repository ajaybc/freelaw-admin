<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "judgement_acts".
 *
 * @property integer $judgement_id
 * @property integer $act_id
 *
 * @property Judgements $judgment
 * @property Acts $act
 */
class JudgementActs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'judgement_acts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judgement_id', 'act_id'], 'required'],
            [['judgement_id', 'act_id'], 'integer'],
            [['judgement_id', 'act_id'], 'unique', 'targetAttribute' => ['judgement_id', 'act_id'], 'message' => 'The combination of Judgment ID and Act ID has already been taken.'],
            [['judgement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Judgements::className(), 'targetAttribute' => ['judgement_id' => 'id']],
            [['act_id'], 'exist', 'skipOnError' => true, 'targetClass' => Acts::className(), 'targetAttribute' => ['act_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'judgement_id' => 'Judgement ID',
            'act_id' => 'Act ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgment()
    {
        return $this->hasOne(Judgements::className(), ['id' => 'judgement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAct()
    {
        return $this->hasOne(Acts::className(), ['id' => 'act_id']);
    }
}
