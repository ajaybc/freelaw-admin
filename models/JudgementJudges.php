<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "judgement_judges".
 *
 * @property integer $judgement_id
 * @property integer $judge_id
 *
 * @property Judgements $judgement
 * @property Judges $judge
 */
class JudgementJudges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'judgement_judges';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judgement_id', 'judge_id'], 'required'],
            [['judgement_id', 'judge_id'], 'integer'],
            [['judgement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Judgements::className(), 'targetAttribute' => ['judgement_id' => 'id']],
            [['judge_id'], 'exist', 'skipOnError' => true, 'targetClass' => Judges::className(), 'targetAttribute' => ['judge_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'judgement_id' => 'Judgement ID',
            'judge_id' => 'Judge ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgement()
    {
        return $this->hasOne(Judgements::className(), ['id' => 'judgement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudge()
    {
        return $this->hasOne(Judges::className(), ['id' => 'judge_id']);
    }
}
