<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "judgement_citations".
 *
 * @property integer $judgement_id
 * @property integer $citation_id
 *
 * @property Judgements $judgement
 * @property JournalCitations $citation
 */
class JudgementCitations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'judgement_citations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judgement_id', 'citation_id'], 'required'],
            [['judgement_id', 'citation_id'], 'integer'],
            [['judgement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Judgements::className(), 'targetAttribute' => ['judgement_id' => 'id']],
            [['citation_id'], 'exist', 'skipOnError' => true, 'targetClass' => JournalCitations::className(), 'targetAttribute' => ['citation_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'judgement_id' => 'Judgement ID',
            'citation_id' => 'Citation ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgement()
    {
        return $this->hasOne(Judgements::className(), ['id' => 'judgement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitation()
    {
        return $this->hasOne(JournalCitations::className(), ['id' => 'citation_id']);
    }
}
