<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "judgements".
 *
 * @property integer $id
 * @property integer $court_id
 * @property integer $type_id
 * @property integer $number
 * @property integer $year
 * @property string $decided_on
 * @property string $appellants
 * @property string $respondents
 * @property string $subject
 * @property string $catchwords
 * @property string $disposition
 * @property string $judgement
 * @property integer $created_by
 * @property string $created
 * @property string $updated
 *
 * @property Headnotes[] $headnotes
 * @property JudgementActs[] $judgementActs
 * @property Acts[] $acts
 * @property JudgementCitations[] $judgementCitations
 * @property JudgementJudges[] $judgementJudges
 * @property AdminUsers $createdBy
 * @property Courts $court
 * @property JudgementTypes $type
 */
class Judgements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'judgements';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['court_id', 'type_id', 'number', 'year', 'appellants', 'respondents', 'subject', 'disposition', 'judgement', 'created_by'], 'required'],
            [['number'], 'unique', 'targetAttribute' => ['type_id', 'number'], 'message' => 'A judgement with the same case type and number already exists'],
            [['court_id', 'type_id', 'number', 'year', 'created_by'], 'integer'],
            [['decided_on', 'created', 'updated'], 'safe'],
            [['catchwords', 'disposition', 'judgement'], 'string'],
            [['appellants', 'respondents', 'subject'], 'string', 'max' => 500],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => AdminUsers::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['court_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courts::className(), 'targetAttribute' => ['court_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => JudgementTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'court_id' => 'Court ID',
            'type_id' => 'Case Type',
            'number' => 'Case Number',
            'year' => 'Year',
            'decided_on' => 'Decided On',
            'appellants' => 'Appellants',
            'respondents' => 'Respondents',
            'subject' => 'Subject',
            'catchwords' => 'Catchwords',
            'disposition' => 'Disposition',
            'judgement' => 'Judgement',
            'created_by' => 'Created By',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHeadnotes()
    {
        return $this->hasMany(Headnotes::className(), ['judgement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgementActs()
    {
        return $this->hasMany(JudgementActs::className(), ['judgement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActs()
    {
        return $this->hasMany(Acts::className(), ['id' => 'act_id'])->viaTable('judgement_acts', ['judgement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCitations()
    {
        return $this->hasMany(JournalCitations::className(), ['id' => 'citation_id'])->viaTable('judgement_citations', ['judgement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudges()
    {
        return $this->hasMany(Judges::className(), ['id' => 'judge_id'])->viaTable('judgement_judges', ['judgement_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(AdminUsers::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourt()
    {
        return $this->hasOne(Courts::className(), ['id' => 'court_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(JudgementTypes::className(), ['id' => 'type_id']);
    }

    /**
     * Create a judgement
     * @param  Array $data Judgement Info
     * @return Array       Status of the operation.Contains error messages if any
     */
    public function create($data)
    {
        //$connection = yii\db\Connection;
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $this->attributes = [
            'court_id' => $data['court_id'],
            'type_id' => $data['type_id'],
            'number' => $data['number'],
            'year' => $data['year'],
            'decided_on' => $data['decided_on'],
            'appellants' => $data['appellants'],
            'respondents' => $data['respondents'],
            'subject' => $data['subject'],
            'catchwords' => $data['catchwords'],
            'disposition' => $data['disposition'],
            'judgement' => $data['judgement'],
            'created_by' => Yii::$app->user->id
        ];

        if ($this->save()) {
            $judgementId = $this->id;
        } else {
            $transaction->rollBack();
            return [
                'status' => false,
                'errors' => $this->getErrors()
            ];
        }

        if ($data['acts']) {
            foreach ($data['acts'] as $act) {
                $existingAct = Acts::find()->where($act)->one();
                if (!$existingAct) {
                    //Means this exact same act is not there in the db. We will need to create a new one
                    $actModel = new Acts;
                    $actModel->attributes = [
                        'book_id' => $act['book_id'],
                        'year' => $act['year'],
                        'chapter' => $act['chapter'],
                        'part' => $act['part'],
                        'section' => $act['section'],
                        'order_number' => $act['order_number'],
                        'rule' => $act['rule']
                    ];
                    if (!$actModel->save()) {
                        $transaction->rollBack();
                        return [
                            'status' => false,
                            'errors' => $actModel->getErrors()
                        ];
                    }
                }

                $actId = ($existingAct->id)?$existingAct->id:$actModel->id;

                $jActModel = new JudgementActs;
                $jActModel->attributes = [
                    'judgement_id' => $judgementId,
                    'act_id' => $actId
                ];
                if(!$jActModel->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => false,
                        'errors' => $jActModel->getErrors()
                    ];
                }
            }
        }

        if ($data['judge_ids']) {
            foreach ($data['judge_ids'] as $judgeId) {
                $jJudgeModel = new JudgementJudges;
                $jJudgeModel->attributes = [
                    'judgement_id' => $judgementId,
                    'judge_id' => $judgeId
                ];
                if(!$jJudgeModel->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => false,
                        'errors' => $jJudgeModel->getErrors()
                    ];
                }
            }
        }

        if ($data['citations']) {
            foreach ($data['citations'] as $citation) {
                $existingCitation = JournalCitations::find()->where($citation)->one();
                if (!$existingCitation) {
                    //Means this exact same act is not there in the db. We will need to create a new one
                    $citModel = new JournalCitations;
                    $citModel->attributes = $citation;
                    if (!$citModel->save()) {
                        $transaction->rollBack();
                        return [
                            'status' => false,
                            'errors' => $citModel->getErrors()
                        ];
                    }
                }

                $citId = ($existingCitation->id)?$existingCitation->id:$citModel->id;

                $jCitModel = new JudgementCitations;
                $jCitModel->attributes = [
                    'judgement_id' => $judgementId,
                    'citation_id' => $citId
                ];
                if(!$jCitModel->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => false,
                        'errors' => $jCitModel->getErrors()
                    ];
                }
            }
        }

        $transaction->commit();
        return [
            'status' => true,
            'id' => $judgementId
        ];
    }


    public function getDetailsForEdit($id)
    {
        $judgementModel = Judgements::find($id)->with('acts', 'judges', 'citations')->one();
        $judgement = \yii\helpers\ArrayHelper::toArray($judgementModel);
        $acts = \yii\helpers\ArrayHelper::toArray($judgementModel->acts);
        $citations = \yii\helpers\ArrayHelper::toArray($judgementModel->citations);
        $judge_ids = \yii\helpers\ArrayHelper::toArray($judgementModel->judges);
        if ($judge_ids) {
            $judge_ids = array_column($judge_ids, 'id');
        }

        //We need to do some hacky type conversion to make this data work with angular select
        $judgement['court_id'] = strval($judgement['court_id']);
        $judgement['type_id'] = strval($judgement['type_id']);
        $judgement['year'] = strval($judgement['year']);

        for ($i = 0; $i < count($acts); $i++) {
            $acts[$i]['book_id'] = strval($acts[$i]['book_id']);
        }

        for ($i = 0; $i < count($citations); $i++) {
            $citations[$i]['journal_id'] = strval($citations[$i]['journal_id']);
        }

        return array_merge($judgement, ['acts' => $acts], ['citations' => $citations], ['judge_ids' => $judge_ids]);
    }

    /**
     * edit a judgement
     * @param  Array $data Judgement Info
     * @return Array       Status of the operation.Contains error messages if any
     */
    public function edit($data)
    {
        //stop($data);
        //$connection = yii\db\Connection;
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $judgementId = $data['id'];
        $judgementModel = Judgements::findOne($judgementId);

        $judgementModel->attributes = [
            'court_id' => $data['court_id'],
            'type_id' => $data['type_id'],
            'number' => $data['number'],
            'year' => $data['year'],
            'decided_on' => $data['decided_on'],
            'appellants' => $data['appellants'],
            'respondents' => $data['respondents'],
            'subject' => $data['subject'],
            'catchwords' => $data['catchwords'],
            'disposition' => $data['disposition'],
            'judgement' => $data['judgement']
        ];

        if (!$judgementModel->save()) {
            $transaction->rollBack();
            return [
                'status' => false,
                'errors' => $judgementModel->getErrors()
            ];
        }

        $this->__deleteExistingActs($judgementId);

        if ($data['acts']) {
            foreach ($data['acts'] as $act) {
                $existingAct = Acts::find()->where($act)->one();
                if (!$existingAct) {
                    //Means this exact same act is not there in the db. We will need to create a new one
                    $actModel = new Acts;
                    $actModel->attributes = [
                        'book_id' => $act['book_id'],
                        'year' => $act['year'],
                        'chapter' => $act['chapter'],
                        'part' => $act['part'],
                        'section' => $act['section'],
                        'order_number' => $act['order_number'],
                        'rule' => $act['rule']
                    ];
                    if (!$actModel->save()) {
                        $transaction->rollBack();
                        return [
                            'status' => false,
                            'errors' => $actModel->getErrors()
                        ];
                    }
                }

                $actId = ($existingAct->id)?$existingAct->id:$actModel->id;

                $jActModel = new JudgementActs;
                $jActModel->attributes = [
                    'judgement_id' => $judgementId,
                    'act_id' => $actId
                ];
                if(!$jActModel->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => false,
                        'errors' => $jActModel->getErrors()
                    ];
                }
            }
        }

        $this->__deleteExistingJudges($judgementId);

        if ($data['judge_ids']) {
            foreach ($data['judge_ids'] as $judgeId) {
                $jJudgeModel = new JudgementJudges;
                $jJudgeModel->attributes = [
                    'judgement_id' => $judgementId,
                    'judge_id' => $judgeId
                ];
                if(!$jJudgeModel->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => false,
                        'errors' => $jJudgeModel->getErrors()
                    ];
                }
            }
        }


        $this->__deleteExistingCitations($judgementId);

        if ($data['citations']) {
            foreach ($data['citations'] as $citation) {
                $existingCitation = JournalCitations::find()->where($citation)->one();
                if (!$existingCitation) {
                    //Means this exact same act is not there in the db. We will need to create a new one
                    $citModel = new JournalCitations;
                    $citModel->attributes = $citation;
                    if (!$citModel->save()) {
                        $transaction->rollBack();
                        return [
                            'status' => false,
                            'errors' => $citModel->getErrors()
                        ];
                    }
                }

                $citId = ($existingCitation->id)?$existingCitation->id:$citModel->id;

                $jCitModel = new JudgementCitations;
                $jCitModel->attributes = [
                    'judgement_id' => $judgementId,
                    'citation_id' => $citId
                ];
                if(!$jCitModel->save()) {
                    $transaction->rollBack();
                    return [
                        'status' => false,
                        'errors' => $jCitModel->getErrors()
                    ];
                }
            }
        }

        $transaction->commit();
        return [
            'status' => true,
            'id' => $judgementId
        ];
    }


    private function __deleteExistingActs($judgementId)
    {
        JudgementActs::deleteAll('judgement_id = :judgementId', [':judgementId' => $judgementId]);
    }

    private function __deleteExistingJudges($judgementId)
    {
        JudgementJudges::deleteAll('judgement_id = :judgementId', [':judgementId' => $judgementId]);
    }

    private function __deleteExistingCitations($judgementId)
    {
        JudgementCitations::deleteAll('judgement_id = :judgementId', [':judgementId' => $judgementId]);
    }
}
