<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "courts".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Judgements[] $judgements
 */
class Courts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'courts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgements()
    {
        return $this->hasMany(Judgements::className(), ['court_id' => 'id']);
    }
}
