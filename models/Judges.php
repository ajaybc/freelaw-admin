<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "judges".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $created
 */
class Judges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'judges';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['created'], 'safe'],
            [['first_name', 'last_name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'created' => 'Created',
        ];
    }
}
