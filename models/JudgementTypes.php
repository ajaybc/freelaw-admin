<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "judgement_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $created
 * @property string $updated
 *
 * @property Judgements[] $judgements
 */
class JudgementTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'judgement_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['code'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgements()
    {
        return $this->hasMany(Judgements::className(), ['type_id' => 'id']);
    }
}
