<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "journal_citations".
 *
 * @property integer $id
 * @property integer $journal_id
 * @property integer $year
 * @property integer $volume
 * @property integer $page
 * @property string $created
 * @property string $updated
 *
 * @property Journals $journal
 * @property JudgementCitations[] $judgementCitations
 */
class JournalCitations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'journal_citations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['journal_id', 'year', 'volume', 'page'], 'required'],
            [['journal_id', 'year', 'volume', 'page'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['journal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Journals::className(), 'targetAttribute' => ['journal_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'journal_id' => 'Journal ID',
            'year' => 'Year',
            'volume' => 'Volume',
            'page' => 'Page',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJournal()
    {
        return $this->hasOne(Journals::className(), ['id' => 'journal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgementCitations()
    {
        return $this->hasMany(JudgementCitations::className(), ['citation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudgments()
    {
        return $this->hasMany(Judgements::className(), ['id' => 'judgment_id'])->viaTable('judgement_citations', ['citation_id' => 'id']);
    }
}
