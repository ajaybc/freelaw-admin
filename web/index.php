<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

if (file_exists(__DIR__ . '/../config/production/web.php')) {
    $config = require(__DIR__ . '/../config/production/web.php');
} else if (file_exists(__DIR__ . '/../config/dev/web.php')){
    $config = require(__DIR__ . '/../config/dev/web.php');
} else {
    exit('No production or dev config found. Copy and customise the config from sample folder');
}

(new yii\web\Application($config))->run();

function pre($data) {
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function stop($data) {
    pre($data);
    exit;
}
