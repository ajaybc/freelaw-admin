'use strict';
angular.module('freelaw', ['ui.tinymce', '720kb.datepicker', 'ngMessages'])
    .config(['$qProvider', function ($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }])
    .controller('judgementController', ['$scope', '$window', 'courtsService', 'judgementsService', 'judgesService',
                function ($scope, $window, courtsService, judgementsService, judgesService) {
        $scope.formData = {
            judge_ids : [],
            acts : [],
            citations : [],
        };

        /*
        If called, will fetch the formdata for a particular judgement. Useful for edit page
         */
        $scope.preloadData = function (judgementId) {
            $scope.showLoading = true;
            judgementsService.getDataForEdit(judgementId)
                .then(function (data) {
                    $scope.formData = data;
                    $scope.showLoading = false;
                })
        }

        $scope.submit = function ($event) {
            $event.preventDefault();
            $scope.showLoading = true;
            if ($scope.form.$invalid) {
                alert('Please enter valid data');
                return;
            }
            judgementsService.submit($scope.formData).then(function (response) {
                //Means Judgement was inserted/edited successfully
                $window.location.href = '/judgements';
            }).catch(function (response) {
                if (response.status == 400) {
                    var errorString = '';
                    if (response.data.errors) {
                        var errorKeys = Object.keys(response.data.errors);
                        for (var ppty in response.data.errors) {
                            errorString += response.data.errors[ppty][0] + '\n';
                        }
                    }
                    alert(errorString);
                } else {
                    alert('Something has gone wrong. Contact Ajay immediatly');
                }
                $scope.showLoading = false;
            })
        }

        /**
         * JudgePicker
         */
        $scope.judges = [];
        judgesService.getJudges().then(function (judges) {
            $scope.judges = judges;
        });

        $scope.clickJudge = function (judgeId) {
            var index = $scope.formData.judge_ids.indexOf(judgeId);
            if (index > -1) {
                //Means this judge is already selected. So remove
                $scope.formData.judge_ids.splice($scope.formData.judge_ids.indexOf(judgeId), 1);
            } else {
                $scope.formData.judge_ids.push(judgeId);
            }
        }


        $scope.unselectJudges = function () {
            $scope.formData.judge_ids = [];
        }

        $scope.selectAllJudges = function (judgeId) {
            $scope.formData.judge_ids = [];
            for (var i = 0; i < $scope.judges.length; i++) {
                $scope.formData.judge_ids.push($scope.judges[i].id);
            }
        }


        $scope.clearJudgeFilter = function () {
            $scope.judgeFilter = '';
        }


        /**
         * Acts
         */

        $scope.addAct = function () {
            $scope.formData.acts.push({});
        }

        $scope.removeAct = function ($index) {
            $scope.formData.acts.splice($index, 1);
        }

        /**
         * Citations
         */

        $scope.addCitation = function () {
            $scope.formData.citations.push({});
        }

        $scope.removeCitation = function ($index) {
            $scope.formData.citations.splice($index, 1);
        }

        /**
         * Tinymce
         */

        $scope.tinymceOptions = {
            plugins: 'link image code',
            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code',
            height: "600px",
            theme : "modern",
            forced_root_block : ""
        }
    }])
    .service('judgementsService', ['$http', function ($http) {
        this.getCaseTypes = function () {
            return $http.get('/judgements/casetypesjson')
                .then(function (response) {
                    return response.data
                })
        }

        this.getDataForEdit = function (judgementId) {
            return $http.get('/judgements/editdatajson?id=' + judgementId)
                .then(function (response) {
                    return response.data
                })
        }

        this.submit = function (formData) {
            var url = (formData.id)?('/judgements/edit?id=' + formData.id):'/judgements/create';
            return $http({
                method: 'POST',
                url: url,
                data: $.param(formData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
        }
    }])
    .service('courtsService', ['$http', function ($http) {
        this.getCourts = function () {
            return $http.get('/courts/courtsjson')
                .then(function (response) {
                    return response.data
                })
        }
    }])
    .service('judgesService', ['$http', function ($http) {
        this.getJudges = function () {
            return $http.get('/judges/judgesjson')
                .then(function (response) {
                    return response.data
                })
        }
    }]);
